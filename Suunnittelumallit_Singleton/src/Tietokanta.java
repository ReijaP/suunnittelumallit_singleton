
public final class Tietokanta {
	
	private static volatile Tietokanta instance = null;
	
	private Tietokanta() {
	}
	
	public static Tietokanta getInstance() {
		if (instance == null){
			synchronized(Tietokanta.class) {
				if (instance == null) {
					instance = new Tietokanta();
				}
			}
		}
		return instance;
	}
	
	public void yhdistaTietokantaan() {
		System.out.println("Yhteyttä muodostetaan...");
	}

}
